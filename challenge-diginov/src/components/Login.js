import { useState } from "react"
import { Link } from "react-router-dom";
import './Register.css'

const Login = ()=> {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
   
    // States for checking the errors
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);
   
    // Handling the email change
    const handleEmail = (e) => {
      setEmail(e.target.value);
      setSubmitted(false);
    };
   
    // Handling the password change
    const handlePassword = (e) => {
      setPassword(e.target.value);
      setSubmitted(false);
    };
   
    // Handling the form submission
    const handleSubmit = (e) => {
      e.preventDefault();
      if ( email === '' || password === '') {
        setError(true);
      } else {
        setSubmitted(true);
        setError(false);
      }
    };
   
 
   
    // Showing error message if error is true
    const errorMessage = () => {
      return (
        <div
          className="error"
          style={{
            display: error ? '' : 'none',
          }}>
          <h1>Please check your email or your password</h1>
        </div>
      );
    };
   
    return (
      <div className="main-div-reg"> 
      <div className="form">
        <div className="title-login">
          <h1>Login </h1>
        </div>
   
        <form>
   
          <label className="label">Email</label>
          <input onChange={handleEmail} className="input"
            value={email} type="email" />
   
          <label className="label">Password</label>
          <input onChange={handlePassword} className="input"
            value={password} type="password" />
   
          <button onClick={handleSubmit} className="btn-register" type="submit">
            Login
          </button>
        </form>

        <h4>
          <Link to="/">
          you don't have account
          </Link>
          </h4>

         {/* Calling to the methods */}
         <div className="messages">
          {errorMessage()}
        </div>
      </div>
      </div>
    );
  
}

export default Login 