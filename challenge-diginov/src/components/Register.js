import { useState } from "react"
import { Link } from "react-router-dom";
import './Register.css'

const Register = ()=> {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
   
    // States for checking the errors
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);
   
    // Handling the name change
    const handleName = (e) => {
      setName(e.target.value);
      setSubmitted(false);
    };
   
    // Handling the email change
    const handleEmail = (e) => {
      setEmail(e.target.value);
      setSubmitted(false);
    };
   
    // Handling the password change
    const handlePassword = (e) => {
      setPassword(e.target.value);
      setSubmitted(false);
    };
   
    // Handling the form submission
    const handleSubmit = (e) => {
      e.preventDefault();
      if (name === '' || email === '' || password === '') {
        setError(true);
      } else {
        setSubmitted(true);
        setError(false);
      }
    };
   
    // Showing success message
    const successMessage = () => {
      return (
        <div
          className="success"
          style={{
            display: submitted ? '' : 'none',
          }}>
          <h1>User {name} successfully registered!!</h1>
        </div>
      );
    };
   
    // Showing error message if error is true
    const errorMessage = () => {
      return (
        <div
          className="error"
          style={{
            display: error ? '' : 'none',
          }}>
          <h1>Please enter all the fields</h1>
        </div>
      );
    };
   
    return (
      <div className="main-div-reg"> 
        <div className="form">
        <div className="title-register">
          <h1>Welcome to the Project Management </h1>
        </div>
   
        <form>
          {/* Labels and inputs for form data */}
          <label className="label">Name</label>
          <input onChange={handleName} className="input"
            value={name} type="text" />
   
          <label className="label">Email</label>
          <input onChange={handleEmail} className="input"
            value={email} type="email" />
   
          <label className="label">Password</label>
          <input onChange={handlePassword} className="input"
            value={password} type="password" />

            <div >
            <input type="radio" name="flexRadioDefault" id="flexRadioDefault1"/>
            <label for="flexRadioDefault1">
                Project Manager
            </label>
            </div>
            <div >
            <input  type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked/>
            <label  for="flexRadioDefault2">
                User
            </label>
            </div>
   
          <button onClick={handleSubmit} className="btn-register" type="submit">
            Register
          </button>
        </form>

        <h4>
          <Link to="/login">
           you have already account
          </Link>
         </h4>

         {/* Calling to the methods */}
         <div className="messages">
          {errorMessage()}
          {successMessage()}
        </div>
      </div>

      </div>
     
    );
  
}

export default Register 